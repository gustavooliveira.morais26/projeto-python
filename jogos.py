import forca
import adivinhacao

def escolhe_jogo():
    print("*************************************")
    print("********* Escolha Seu jogo! *********")
    print("*************************************")
    print("1-Jogo da Forca \n2-Jogo da Adivinhação")
    jogo = int(input("Selecione seu jogo: "))
    if (jogo == 1):
        print("Executando Forca...")
        forca.jogar()
    elif(jogo == 2):
        print("Executando Adivinhação...")
        adivinhacao.jogar()
    else:
        print("Opção inválida!")

if(__name__ == "__main__"):
    escolhe_jogo()
