import random
def jogar():
    print("*************************************")
    print("**Bem vindo ao jogo da adivinhação!**")
    print("*************************************")

    num_secreto = random.randrange(1,101)
    pontos = 1000

    print("Níveis de Dificuldade")
    print("1-Fácil    2-Médio    3-Difícil")
    nivel = int(input("Selecione o nível: "))
    if(nivel == 1):
        tentativas = 20
    elif(nivel == 2):
        tentativas = 10
    else:
        tentativas = 5


    for rodada in range(1,tentativas+1):
        print("---"*20)
        print("Tentativa {} de {}".format(rodada,tentativas))
        chute = int(input("Digite um número de 1 a 100: "))
        print("Você digitou: ", chute)

        if(chute<1 or chute>100):
            print("Você deve digitar um número entre 1 e 100!")
            continue

        acertou = chute ==num_secreto
        maior   = chute > num_secreto
        menor   = chute < num_secreto
        if(acertou):
            print("Você acertou! \nPontuação: {}".format(pontos))
            break
        else:
            if(maior):
                print("Você errou! Seu chute foi maior que o número secreto!")
            elif(menor):
                print("Você errou! Seu chute foi menor que o número secreto!")
            rodada += 1
            pontos -= abs(num_secreto - chute)
    print("Fim do jogo...")

if(__name__ == "__main__"):
    jogar()